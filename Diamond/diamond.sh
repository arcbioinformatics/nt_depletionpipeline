UNMAPPED_FASTQ_PATHS=$1
DIAMOND_INPUT_FASTQ_DIR=$(dirname $UNMAPPED_FASTQ_PATHS)

if [ ! -f ${DIAMOND_INPUT_FASTQ_DIR}/diamond_input_concatenated_unmapped.diamond.daa ]; then
	tag_reads() {
		d=$(dirname $1)
		b=$(basename $1 .GRCh38.fa.unmapped.fastq.gz)
		zcat $1 | sed "1~4s/$/___$b/" | tee >(gzip > ${d}/${b}.GRCh38.fa.unmapped.tagged.fastq.gz) >(wc -l | awk -vlib=$b '{print lib, $1/4}' > ${d}/${b}.unmappedreadcount.txt) 1> /dev/null
	}
	export -f tag_reads
	parallel -j4 tag_reads ::: $(cat $UNMAPPED_FASTQ_PATHS)
	
	cat ${DIAMOND_INPUT_FASTQ_DIR}/*.unmappedreadcount.txt | sort > ${DIAMOND_INPUT_FASTQ_DIR}/diamond_inputreadcount.txt
	rm -f ${DIAMOND_INPUT_FASTQ_DIR}/*.unmappedreadcount.txt
	
	# concate all tagged files into one for ONE diamond run...
	find $DIAMOND_INPUT_FASTQ_DIR -name "*.GRCh38.fa.unmapped.tagged.fastq.gz" | sort | xargs -I xxx cat xxx > ${DIAMOND_INPUT_FASTQ_DIR}/diamond_input_concatenated_unmapped.fastq.gz
	
	# ...and remove individual tagged files
	find $DIAMOND_INPUT_FASTQ_DIR -name "*.GRCh38.fa.unmapped.tagged.fastq.gz" | xargs rm
	
	# Now to the actual DIAMOND run if daa does not exist, store the output and error logs

	DIAMOND_DB="/tars01/data/ref/diamond_taxid/nr_tax"	
	/mnt/bdbStorage01/programs/diamond blastx -p 32 -d $DIAMOND_DB -q ${DIAMOND_INPUT_FASTQ_DIR}/diamond_input_concatenated_unmapped.fastq.gz -o ${DIAMOND_INPUT_FASTQ_DIR}/diamond_input_concatenated_unmapped.diamond -f 100 -b 24.0 > ${DIAMOND_INPUT_FASTQ_DIR}/diamond.out 2>${DIAMOND_INPUT_FASTQ_DIR}/diamond.err

else
	echo "${DIAMOND_INPUT_FASTQ_DIR}/diamond_input_concatenated_unmapped.diamond.daa exists, not rerunning Diamond"
fi




# Distribute the results out 

for f in $(/mnt/bdbStorage01/programs/diamond view -a ${DIAMOND_INPUT_FASTQ_DIR}/diamond_input_concatenated_unmapped.diamond.daa | cut -f 1 | sed 's/___/\t/' | awk '(!arr[$2]++){print $2}'); do
	/mnt/bdbStorage01/programs/diamond view -a ${DIAMOND_INPUT_FASTQ_DIR}/diamond_input_concatenated_unmapped.diamond.daa | grep $f > ${DIAMOND_INPUT_FASTQ_DIR}/${f}.diamondout.txt
	diamond_reads=$(cat ${DIAMOND_INPUT_FASTQ_DIR}/${f}.diamondout.txt | awk '!arr[$1]++' | wc -l )
	echo $f $diamond_reads > ${DIAMOND_INPUT_FASTQ_DIR}/${f}.diamondreads.out
done

cat ${DIAMOND_INPUT_FASTQ_DIR}/*.diamondreads.out | sort > ${DIAMOND_INPUT_FASTQ_DIR}/diamond_reads.txt
rm -f ${DIAMOND_INPUT_FASTQ_DIR}/*.diamondreads.out ${DIAMOND_INPUT_FASTQ_DIR}/*.diamondout.txt

RUNDIR=$(dirname $DIAMOND_INPUT_FASTQ_DIR)
cat ${RUNDIR}/multiqc_data/multiqc_general_stats.txt | cut -f 1,5 | tail -n +2 | sed 's/_R[12]_001//' | awk '{arr[$1]+=$2}END{for(i in arr){print i, arr[i]}}'  | sort | awk 'FNR==NR{hash[$1]=$2;next}($1 in hash){print $0, hash[$1]}' - ${DIAMOND_INPUT_FASTQ_DIR}/diamond_reads.txt | awk 'FNR==NR{hash[$1]=$2; next}($1 in hash){print $1, $2, hash[$1], $3}' ${DIAMOND_INPUT_FASTQ_DIR}/diamond_inputreadcount.txt - | awk '{print $0, 100*$2/$4, 100*$3/$4 }' | sed 's/ /\t/g' > ${DIAMOND_INPUT_FASTQ_DIR}/diamond_reads_total.txt

                        #total_unmapped_reads=$(zcat ${out_dir}/${pre}.${index_abbr}_unmapped_R1_001.fastq.gz | wc -l | awk'{print $1/4}')
                        #total_unmapped_diamond_reads=$(/mnt/bdbStorage01/programs/diamond view -a ${out_dir}/${pre}.${index_abbr}_unmapped_R1_diamond.daa | cut -f 1 | awk '!arr[$1]++' | wc -l)
                        #echo ${pre}.${index_abbr} $total_unmapped_reads $total_unmapped_diamond_reads > ${out_dir}/${pre}.${index_abbr}_unmapped_R1_diamond_mapped.txt
