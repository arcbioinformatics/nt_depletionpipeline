
pre=${1/.bam/}
java -jar ${NT_DEP_SCRIPT_DIR}/Inserts/picard.jar MarkDuplicates I=$1 O=${pre}.rmdup.bam M=${pre}_dup_metrics.txt VALIDATION_STRINGENCY=LENIENT REMOVE_DUPLICATES=true TMP_DIR=$tmpDir
