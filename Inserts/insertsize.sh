bam=$1
prefix=${bam/.bam/}

#java -jar /home/sradhakrishnan/programs/picard/build/libs/picard.jar CollectInsertSizeMetrics I=$bam O=${prefix}.IS.txt H=${prefix}.histogram.pdf #M=0.5
java -jar ${NT_DEP_SCRIPT_DIR}/Inserts/picard.jar CollectInsertSizeMetrics I=$bam O=${prefix}.IS.txt H=${prefix}.histogram.pdf > ${prefix}.IS.STDOUT 2> ${prefix}.IS.STDERR 
