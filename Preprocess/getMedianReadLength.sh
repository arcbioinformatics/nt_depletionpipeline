


getRL() {

    LIB=`basename $1 _R1_001.fastq.gz`
    median_read_length=`zcat $1 |  sed -n '2~4p' | awk '{print length($1)}' | head -n 10000 | sort -n | head -n 25001 | tail -n 1`
    echo $LIB $median_read_length >> ${LIB}.MRL
}
export -f getRL
parallel -j0 getRL ::: $(cat $1 | cut -f 1 -d " ")

cat *.MRL | sort | uniq > ${1}_medianreadlength.tsv
rm -f *.MRL

mkdir -p fastqc_analysis
fastqc_method() {
    FILE_PRE=`basename $1 _001.fastq.gz`
    echo "Creating output dir $FILE_PRE"
    mkdir -p fastqc_analysis/$FILE_PRE
    /tars00/sradhakrishnan/programs/fastqc/FastQC/fastqc --nogroup -o fastqc_analysis/$FILE_PRE -t 6 $1
}

export -f fastqc_method
parallel --record-env

if [ ! -f "multiqc_report.html" ]; then 
	parallel -j10 fastqc_method ::: $(cat $1 | sed 's/ /\n/' | sort)
	multiqc -f fastqc_analysis
fi
