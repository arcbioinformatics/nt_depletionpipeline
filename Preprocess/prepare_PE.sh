# This script should read the design.txt file and flagstat files and create a data_v2 file that stores mapped read numbers along with control_set/index.

design=$1
dir=$2
basedir=`basename $2`

while IFS= read line
do
    #IFS=$'\t' 
    read -r -a array <<< "$line"
    sample="${array[0]}"
    control_set="${array[1]}"
    experiment="${array[2]}"
    f=`find $dir -name "${sample}_*.flagstat"`
#    echo $sample 
#    echo $control_set
#    echo $experiment
    echo -ne "${sample} ${basedir} ${control_set} ${experiment} "
    mapped=`grep "properly paired (" $f | cut -f 1 -d " "`;
    total=`grep "paired in sequencing" $f | cut -f 1 -d " "`;
    echo -ne "$mapped "; echo $total;
done < "$design"
