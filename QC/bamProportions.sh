if [ "$#" -ne 1 ]; then
	printf "\n Need config file as input\n" 
    exit 1;
fi

#FASTA="/mnt/bdbStorage01/data/reference/viralgenomes/HHV6A/HHV6A.fa"
#FOLDER="HHV6A_bwamaps"
#BAM="/tars01/tcarter/SEQUENCING_RUNS/Run197/Analysis/HHV6A_bwamaps/Plasma_Vir5_NoDep_3_K703_K501_S3.HHV6A.fa.sorted.mappedonly.zszt.ent.bam"
#SUFFIX="zszt.ent.bam"
CONFIG=$1

getSplitupFromBam() {
	bam=$1
	suffix=$2
	folder=$3
	bam_pre=$(basename $bam ".${suffix}")
	sample=$(echo $bam_pre | sed "s/_S[0-9]\+\..*//")
	for f in $(find -L $folder -name "*.${suffix}" | sort); do
		echo -ne "$sample\t$folder\t$suffix\t" > ${sample}_${folder}_${suffix}.bp.txt
		samtools view $bam | awk '{print $10}' | sed 's/\(.\)/\1\n/g' | awk NF | awk '{a[$1]++}END{for (i in a){print i, a[i]}}' | grep "^A\|^C\|^G\|^T" | awk '{print $2}' | paste -d "\t" - - - - >> ${sample}_${folder}_${suffix}.bp.txt 
	done
}
export -f getSplitupFromBam
parallel --record-env
#parallel -j1 getSplitupFromBam ::: $BAM ::: $SUFFIX ::: $FOLDER 

getBaseSplitup() {
	mkdir -p QC_BamProportions
	fasta=$1
	folder=$2
	suffixes=( zszt.ent.bam zszt.ent.filtered.bam zszt.ent.filtered.rmdup.bam )	
	pre=$(basename $fasta)
	echo -ne "REFERENCE\t$pre\tREFERENCE\t" >> ${folder}.bp.txt
	seqtk comp $fasta | cut -f 3-6 >> ${folder}.bp.txt
	for s in "${suffixes[@]}"; do
		for f in `find -L $folder -name "*.${s}" | sort`; do
			getSplitupFromBam $f $s $folder
		done
	done
	cat <(cat ${folder}.bp.txt) <(cat *${folder}_*.bp.txt | sort) > QC_BamProportions/${folder}.bp.summarized.txt
	rm -f *${folder}*.bp.txt	
}
export -f getBaseSplitup
parallel --record-env
parallel -j5 getBaseSplitup ::: $(cat $CONFIG | awk '($4!="host"){print $3}') :::+ $(cat $CONFIG | awk '($4!="host"){print $2}')
