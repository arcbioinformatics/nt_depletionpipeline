if [ "$#" -ne 2 ]; then
	printf "\n Run command as: bash phd.sh <files_paired> <config.tsv>\n"
	exit 1;
fi

FILES_PAIRED=$1
CONFIG=$2

getmapped() {
	outfolder="PHD_plots"
	mkdir -p ${outfolder}
	local path_folder=$1
	local path_map=$2
	local host_folder=$3
	local host_map=$4
	local binstart=$5
	local sample=$6
	local bam_file_host="${host_folder}/${sample}.${host_map}.sorted.mappedonly.bam"
	local bam_file_path="${path_folder}/${sample}.${path_map}.sorted.mappedonly.zszt.ent.filtered.bam"
	local binend=$((binstart+50))
	local path_mapped=$(samtools view -h $bam_file_path |  awk -v binstart=$binstart -v binend=$binend 'function abs(x) {return x<0?-x:x} (abs($9)>=binstart && abs($9)<binend) || $1 ~ /^@/' | samtools view -hbS - | samtools flagstat - | cut -f 1 -d " " | head -n 5 | paste -sd" " | awk '{print $5-$2}')
	local host_mapped=$(samtools view -h $bam_file_host |  awk -v binstart=$binstart -v binend=$binend 'function abs(x) {return x<0?-x:x} (abs($9)>=binstart && abs($9)<binend) || $1 ~ /^@/' | samtools view -hbS - | samtools flagstat - | cut -f 1 -d " " | head -n 5 | paste -sd" " | awk '{print $5-$2}')
 	echo $sample $path_map $binstart $binend $path_mapped $host_mapped > ${outfolder}/${sample}.${path_map}.${binstart}_${binend}.phd.txt
}
export -f getmapped



phd() {
	path_folder=$1
	path_map=$2
	host_folder=$3
	host_map=$4
	sample=$5
	start=50
	end=500
	binwidth=50
	echo "Running Pathogen-Human Divide with parameters: $path_folder $path_map $host_folder $host_map $sample"
	$GNU_PARALLEL_EXEC --link -j10 getmapped ::: $path_folder ::: $path_map ::: $host_folder ::: $host_map ::: $(seq $start $binwidth $end) ::: $sample
}
export -f phd
$GNU_PARALLEL_EXEC --record-env
$GNU_PARALLEL_EXEC -j2 phd ::: $(cat $CONFIG | awk '$4!="host"' | awk '{print $2}') :::+ $(cat $CONFIG |  awk '$4!="host"' | awk '{print $3}' | xargs -I xx basename xx) ::: $(cat $CONFIG | awk '$4=="host"' | awk '{print $2}') :::+ $(cat $CONFIG | awk '$4=="host"' | awk '{print $3}' | xargs -I xx basename xx) ::: $(cat $FILES_PAIRED | cut -f 1 -d " " | xargs -I xx basename xx _R1_001.fastq.gz)
