#cat files_paired | cut -f 1 -d " " | xargs -I xx basename xx | sed 's/_S.*//' > samples

for sample in `cat samples`; do
    echo -ne "$sample\t"
    for folder in `find -L $(pwd -P) -mindepth 1 -maxdepth 1 -type d`; do
        base_folder=`basename $folder`;
        if [ -f ${folder}/${sample}*flagstat ]; then
            echo -ne "${base_folder}\t"
            #cat ${folder}/${sample}.*flagstat | grep "mapped (" | awk '{print $5}' | sed -e 's/[(%]//g' | tr '\n' '\t'
           # total=`cat ${folder}/${sample}.*flagstat | head -n 1 | awk '{print $1}'`
           # mapped=`cat ${folder}/${sample}.*flagstat | grep "mapped (" | awk '{print $1}'`
#            cat ${folder}/${sample}_*flagstat | head -n 5 | awk '{print $1}' | paste -d " " - - - - - | awk '{print $5, 100*$5/$1}' | tr '\n' '\t'
            cat ${folder}/${sample}*flagstat | sed -n '2,6p' | cut -f 1 -d " " | paste -d " " - - - - -  | awk '{print $4-$1-$2, $5, 100*($4-$1-$2)/$5}' |  tr '\n' '\t'
        fi
    done
    echo ""
done
