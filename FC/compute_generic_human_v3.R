rm(list=ls())
args = commandArgs(trailingOnly=TRUE)
if(length(args)!=2) {
    stop("Need human_rawdata.txt and $PWD in that order", call.=FALSE)
}

#human_file <- "human_rawdata.txt"
#wd <- "/tars01/sradhakrishnan/data/SequencingRuns/run108/180124_NB501788_0108_AHVMVKAFXX/projects/Depletion_Optimization/Analysis"

human_file <- args[1]
wd <- args[2]

setwd(wd)

#E. coli numbers

files <- c(human_file)

numbers <- sapply(files, simplify=FALSE, function(file) {
    data <- read.delim(file, header=F, sep=" ")
    colnames(data) <- c("SAMPLE", "MAP", "CONTROL_SET", "EXPERIMENT", "MAPPED", "TOTAL")
    data$PERC_MAPPED <- 100*data$MAPPED/data$TOTAL
    
    experiments <- unique(data$EXPERIMENT[which(data$EXPERIMENT!="0")])
    control_sets <- unique(data$CONTROL_SET)
    map <- unique(data$MAP)
    control_percs <- lapply(experiments, function(exp) {
           data[data$CONTROL_SET==unique(data[data$EXPERIMENT==exp & data$MAP==map,]$CONTROL_SET) & data$EXPERIMENT=="0" & data$MAP==map,]$PERC_MAPPED
    })
    
    depleted_percs <- lapply(experiments, function(exp) {
        r <- data[data$MAP==map & data$EXPERIMENT==exp,]$PERC_MAPPED
        r
    })
    names(depleted_percs) <- experiments
    names(control_percs) <- experiments
    list(control_percs, depleted_percs, experiments)
})


### Human specific numbers

human_numbers_ctrl <-  numbers[[1]][-3][[1]]
human_numbers_depl <-  numbers[[1]][-3][[2]]
human_numbers_experiments <- numbers[[1]][[3]]

human_results.df <- do.call(rbind, mapply(function(c, d, e) {
    combinations <- expand.grid(DEPLETED=d, CONTROL=c)
    combinations$PERC_DEPLETION <- 100*(1-(combinations$DEPLETED*(100-combinations$CONTROL))/(combinations$CONTROL*(100-combinations$DEPLETED)))
    combinations$EXPERIMENT <- e
    combinations
}, human_numbers_ctrl, human_numbers_depl, human_numbers_experiments, USE.NAMES=T, SIMPLIFY=F))


#plots
library(ggplot2)
human_plot <- ggplot(human_results.df) +
    geom_boxplot(aes(x=EXPERIMENT, y=PERC_DEPLETION)) +
    geom_jitter(aes(x=EXPERIMENT, y=PERC_DEPLETION, colour=EXPERIMENT), show.legend=F) + 
    theme(legend.text=element_text(size=16, face="bold"), legend.position = "bottom",
              #panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
              axis.title.x = element_text(face="bold", size=18),
              axis.title.y = element_text(face="bold", size=18),
              axis.text.x  = element_text(size=14, face="bold", angle=45, hjust=1),
              axis.text.y  = element_text(size=14, face="bold"),
              plot.margin = margin(0.5, 0.5, 0.5, 0.5, "cm")) +
    labs(x="EXPERIMENT", y="PERCENT DEPLETION (HOST)") 


write.table(human_results.df, "host_results.tsv", sep="\t", quote=F, row.names=F, col.names=T)    
    
H<-10
W <- ifelse(length(unique(human_results.df$EXPERIMENT))>10, 25, 12)
ggsave("HOST_PLOT.pdf", human_plot, width=W, height=H, units="in")

