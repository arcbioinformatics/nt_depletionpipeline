# NT_DepletionPipeline

download: git clone git@bitbucket.org:arcbioinformatics/nt_depletionpipeline.git

If plasma libraries are being run (70 + 14 + 14 + 70 scheme), run the UMI shuffling script as follows:

bash /mnt/bdbStorage01/sradhakrishnan/pipelines/symlink_bcl2fastq/symlink_demux.sh <absolute_path_to_run_directory> <samplesheet.csv>

Remember that the samplesheet.csv file needs the following 5 lines under [Settings]:

======BEGIN====

[Settings],,,,,,,,,

Adapter,AGATCGGAAGAGCACACGTCTGAACTCCAGTCA,,,,,,,,

AdapterRead2,AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT,,,,,,,,

Read1UMILength,6,,,,,,,,

Read2UMILength,6,,,,,,,,

TrimUMI,1,,,,,,,,

=====END======

This pipeline was tested and run with:

Tool (version)

- GNU parallel (20170822) [note: you need a version of GNU parallel that has the --link functionality - older versions may not]
- bedtools (v2.25.0)


If you haven't already installed the following R packages - do so by typing the following:

install.packages("data.table")

install.packages("ggplot2")

install.packages("plyr")

install.packages("reshape2")

install.packages("tibble")


Add git installation directory and a temporary folder for sort operations (this is hopefully a big folder that you have access to and not /tmp/) to .bashrc and source it. 
Use the following ENV variables

export NT_DEP_SCRIPT_DIR=/tars01/sradhakrishnan/git/nt_depletionpipeline

export tmpDir=/tars01/sradhakrishnan/tmp_sort

followed by

$ source ~/.bashrc

You should now be all set up. Here's the how-to on running the pipeline:

https://arc-bio.atlassian.net/wiki/spaces/BF/pages/239304720/Running+the+depletion+pipeline
