### THIS SCRIPT TAKES IN A BED FILE CORRESPONDING TO THE TOP 10% CAS9 CUTSITES
### IN THE HOST GENOME, AND PRODUCES AN FRAGMENTS-GUIDES (FG) PLOT THAT DESCRIBES
### THE NUMBER OF SURVIVING FRAGMENTS AND THE NUMBER OF GUIDES THAT OCCUR IN THOSE
### FRAGMENTS IN THE CONTROL VS CAS9 DEPLETED LIBRARIES. IF THE DEPLETION WORKED 
### WELL, THERE WILL BE FEWER GUIDES IN THE SURVIVING FRAGMENTS IN THE DEPLETED LIBRARIES. 

### METHOD: 1) CONVERT BAM FILE TO BED THAT HOLDS FRAGMENT COORDINATES
###         2) RUN BEDTOOLS TO OBTAIN FRAGMENTS THAT COMPLETELY OVERLAP GUIDE COORDINATES IN PROVIDED BED
###	    3) RUN NUMBERS ON #FRAGMENTS WITH #OVERLAPPING GUIDES, WRITE TO fg.out, THIS WILL BE USED TO DRAW THE FG_PLOT_ALL.pdf GRAPH
###	    4) INDEPENDENTLY FROM 2), COUNT TOTAL NUMBER OF FRAGMENTS THAT OVERLAP GUIDES AND ENUMERATE SURVIVING FRAGMENTS BY CONTROL
###	       AND DEPLETED LIBRARIES. THIS CAN THEN BE PLOTTED IN TWO WAYS: % SURVIVING FRAGMENTS VS LIBRARY TREATMENTS, FOLLOWED BY
###	       % HOST DEPLETION VS LIBRARY TREATMENTS.

FG_FOLDER=${PWD}/fg_plots
if [[ -d "$FG_FOLDER" ]]; then
    echo "fg_plots folder exists, not rerunning. exiting module."
    exit 0
fi
mkdir -p $FG_FOLDER

#bed="/home/sradhakrishnan/python_test/libraries/90k_MR_genomic/90k_MR_genomic_N20s.bed"
bed=$1
HUMAN_FOLDER=$2

## MODULE TAKES IN A BAM FILE AND ONLY RETAIN THE PAIRED END READS; ALSO GENERATE FLAGSTAT FOR NEWLY GENERATED PE.bam FILE.
## MODULE THROWS OUT ANY READ THAT'S NON-COMPLIANT TO THE 'MAPPED-IN-PROPER-PAIR' SCHEME AS ENFORCED BY BWA.
getPE() {
    f=$1
    FG_FOLDER=$2
    b=$(basename $f .bam)
    samtools view -b -q30 -f 2 $f > ${FG_FOLDER}/${b}.PE.bam
    samtools flagstat ${FG_FOLDER}/${b}.PE.bam > ${FG_FOLDER}/${b}.PE.bam.flagstat
}
export -f getPE
parallel --record-env
parallel -j12 getPE ::: `ls ${HUMAN_FOLDER}/*.mappedonly.bam` ::: ${FG_FOLDER}

## JUST IN CASE THE LIBRARIES ARE ALL DIFFERENT SIZES, YOU'LL LIKELY HAVE VARYING NUMBER OF MAPPED READS IN
## THE PE.bam FILE, SO WE DOWNSAMPLE TO THE LOWEST COMMON DENOMINATOR AS DETERMINED BY FLAGSTAT 
## IF THIS NUMBER IS TOO BIG, WE SETTLE ON 4000000. 
toSample=`cat ${FG_FOLDER}/*PE.bam.flagstat | grep "properly paired" | cut -f 1 -d " " | sort -n | head -n 1`
if [ $toSample -gt 4000000 ]; then
    toSample=4000000
fi

echo "Sampling $toSample reads"

## RUN THE SAMPLING AND GENERATE FRAGMENTS BASED ON THE FIRST READ IN PAIR AND WHETHER IT MAPS TO THE FORWARD OR REVERSE STRAND
sample() {
    base=`basename $1 .bam`
    toSample=$2
    FG_FOLDER=$3
    bedtools sample -n $toSample -i $1 > ${FG_FOLDER}/${base}.${toSample}.bam
    #f64 first in pair, F16 not reversed | get REF, LPOS and TLEN | ensure TLEN is reasonable in length | zip & store
    samtools view -F16 -f64 ${FG_FOLDER}/${base}.${toSample}.bam | cut -f 3,4,9 | awk -v OFS='\t' '{if($3>30 && $3 <1000){print $1, $2, ($2+$3), "+"}}' | gzip -c > ${FG_FOLDER}/${base}.${toSample}.F.bed.gz
    #f64 first in pair, f16 reversed | same constraints as above	
    samtools view -f80 ${FG_FOLDER}/${base}.${toSample}.bam | cut -f 3,4,9 | awk -v OFS='\t' '{if($3<-30 && $3 >-1000){print $1, $2, ($2+(-$3)), "-"}}' | gzip -c > ${FG_FOLDER}/${base}.${toSample}.R.bed.gz
    cat ${FG_FOLDER}/${base}.${toSample}.F.bed.gz ${FG_FOLDER}/${base}.${toSample}.R.bed.gz > ${FG_FOLDER}/${base}.${toSample}.FR.bed.gz
    rm -f ${FG_FOLDER}/${base}.${toSample}.F.bed.gz
    rm -f ${FG_FOLDER}/${base}.${toSample}.R.bed.gz
    rm -f ${FG_FOLDER}/${base}.${toSample}.bam
}
export -f sample
parallel --record-env
parallel -j12 sample ::: `ls ${FG_FOLDER}/*.PE.bam` ::: $toSample ::: ${FG_FOLDER}


## COMPUTE 2-COLUMN LIKE SO:
## NUMBER OF SURVIVING FRAGMENTS (CALL THIS X), NUMBER OF GUIDES IN SAID SURVIVING FRAGMENTS (CALL THIS Y).
## THIS WOULD BE READ AS: THERE ARE X SURVIVING FRAGMENTS THAT HAVE Y GUIDES THAT OVERLAPPING THEM.
fg() {
    frag_file=$1
    guide_file=$2
    FG_FOLDER=$3
    b=`basename $frag_file .bed.gz`
    # print guide file to STDOUT | get columns 1-6 only so as to not mess up downstream formatting | run bedtools intersect with -f 1.0 so entire guide coordinates overlap with fragment | print out guide coordinates with strand and fragment coordinates with strand, separated by space (resulting in 2 columns only, the underscore stitches fragment data and guide data together) | first awk: group by fragment and compute {frag_i, number_guides} tuples | second awk: group by number_guides and compute {number_frags, number_guides} tuples | write to file   
    zcat $guide_file | awk 'BEGIN{OFS="\t";}{print $1, $2, $3, $4, $5, $6}'| bedtools intersect -f 1.0 -wo -a - -b $frag_file  |  awk '{print $1"_"$2"_"$3"_"$6, $7"_"$8"_"$9"_"$10}' |  awk '{arr[$2]++}END{for (i in arr){print i, arr[i]}}' | awk '{arr[$2]++}END{for (i in arr){print arr[i], i}}' > ${FG_FOLDER}/${b}.fg.out
}
export -f fg
parallel --record-env
#parallel -j12 fg ::: `ls ${FG_FOLDER}/*.FR.bed.gz` ::: `ls $bed` ::: ${FG_FOLDER}

## MODULE COUNTS NUMBER OF FRAGMENTS OVERLAPPING WITH GUIDE BED FILE
intersect() {
    fragment_file=$1
    guide_file=$2
    FG_FOLDER=$3
    base=`basename $fragment_file .bed.gz`
    bedtools intersect -a $fragment_file -b $guide_file -F 1.0 -u | wc -l > ${FG_FOLDER}/${base}.overlapping.fragmentcount
}
export -f intersect
parallel --record-env
parallel -j12 intersect ::: `ls ${FG_FOLDER}/*.FR.bed.gz` ::: `ls $bed` ::: ${FG_FOLDER}


## FINALLY, SUMMARIZE OVERLAP NUMBERS ACROSS ALL LIBRARIES
for f in `ls ${FG_FOLDER}/*.fragmentcount`; do
    b=`basename $f`
    total=$toSample
    echo -ne "${b}\t${total}\t"
    cat $f;
done | awk 'BEGIN{OFS="\t";}{print $1, $2/2, $3, 100*2*$3/$2}' > ${FG_FOLDER}/overlapsummary

## CLEAN UP SO FOLDER ISN'T BLOATED WITH BAM FILES
rm -f ${FG_FOLDER}/*.PE.bam* 
