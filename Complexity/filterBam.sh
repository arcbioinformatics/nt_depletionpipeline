#!/bin/bash

mapped_read_length_ratio=0
complexity=0
read_length=0

usage() {
    printf "\n\tusage: filterBam.sh -b <bam_file> -s <mapped_read_length_ratio_cutoff> -c <complexity_cutoff> -r <mapped_read_length_cutoff>\n\n"
}

while [ "$1" != "" ]; do
    case $1 in
        -s | --mapped-read-length-ratio )           shift
                                mapped_read_length_ratio=$1
                                ;;
        -c | --complexity )    shift
                               complexity=$1
                                ;;
        -b | --bam )            shift
                                bam_file=$1       
                                ;; 
        -r | --read-length )   shift
                               read_length=$1
                               ;; 
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
                                ;;
    esac
    shift
done

if [ "$bam_file" = "" ]; then
    printf "\n\tNeed input bam file with ZS, ZT and ZE tags set\n"
    usage
    exit 1
fi

DIR=$(dirname $bam_file)
PRE=$(basename $bam_file .bam)

if [ -f ${DIR}/${PRE}.filtered.sam ]; then
    rm -f ${DIR}/${PRE}.filtered.sam
fi

samtools view -h $bam_file | grep "^@" > ${DIR}/${PRE}.filtered.sam
echo "@PG   ID:filterBam.sh PN:filterbam.sh CL:$(pwd -P)/filterBam.sh --bam $bam_file --read-length $read_length --mapped-read-length-ratio $mapped_read_length_ratio --complexity $complexity" >> ${DIR}/${PRE}.filtered.sam
#echo "@CO: input file: $bam_file"
#echo "@CO: mapped read length cutoff: $read_length"
#echo "@CO: mapped read length ratio cutoff: $mapped_read_length_ratio"
#echo "@CO: complexity cutoff: $complexity"
#samtools view $bam_file | awk 'match($0, /ZE:f:0.[[:digit:]]+/) {print substr($0, RSTART, RLENGTH)}'

samtools view $bam_file | \
    awk -v c=$complexity 'match($0, /ZE:f:0.[[:digit:]]+/) { 
    zs = substr($0, RSTART, RLENGTH); 
    sub("ZE:f:","",zs); 
    if(zs >= c) {
        print $0
    }
}' |\

    awk -v rl=$read_length -v mrlr=$mapped_read_length_ratio '$0 ~ /^@/ || match($0, /ZS:i:[[:digit:]]+\tZT:i:[[:digit:]]+/) {
    zszt = substr($0, RSTART, RLENGTH);
    split(zszt,z,"\t");
    sub("ZS:i:","",z[1]);
    sub("ZT:i:","",z[2]);
    if (z[2] >= rl && 1-(z[1]/z[2]) >= mrlr) {
        print $0
    }
}' >> ${DIR}/${PRE}.filtered.sam

samtools view -hbS  ${DIR}/${PRE}.filtered.sam >  ${DIR}/${PRE}.filtered.bam
rm -f ${DIR}/${PRE}.filtered.sam
bash ${NT_DEP_SCRIPT_DIR}/Inserts/removedups.sh ${DIR}/${PRE}.filtered.bam

