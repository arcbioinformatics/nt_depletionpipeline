if [ $# -ne 1 ]; then
    printf "\nUsage: filterBam_wrapper.sh <BAM_DIR>\n\n"
    exit 1;
fi
BAMDIR=$1
par_ent(){
    #echo $1
    PRE=$(basename $1 .bam)
    DIR=$(dirname $1)
    complexity=$2
    read_length=$3
    mapped_length_ratio=$4
    if [ ! -f ${DIR}/${PRE}.zszt.ent.bam ]; then	
	    bash ${NT_DEP_SCRIPT_DIR}/Complexity/addZOptionalFields.sh $1
	    bash ${NT_DEP_SCRIPT_DIR}/Complexity/filterBam.sh --bam ${DIR}/${PRE}.zszt.ent.bam -s $mapped_length_ratio -c $complexity -r $read_length  
    else
        echo "Not running ZEST filtering on ${PRE}.bam since ${PRE}.zszt.ent.bam exists in ${DIR}"	
    fi
}
export -f par_ent
parallel --record-env
parallel -j1 par_ent ::: `ls $BAMDIR/*.mappedonly.bam` ::: 0.78 ::: 40 ::: 0.70


B=`basename $BAMDIR`
echo -ne "LIBRARY\tRMDUP_FILTERED_READS\tFILTERED_READS\tMAPPED_READS\tTOTAL\tRMDUP_FILTERED\tFILTERED\tMAPPED\n" > ${BAMDIR}/${B}_OVERALLMAPSUMMARY.tsv

for f in `find -L $BAMDIR -name "*.filtered.bam" | sort`; do
    pre=${f/.bam/}
    base=${f/.mappedonly.zszt.ent.filtered.bam/}
    mapped=$(samtools flagstat ${base}.mappedonly.bam | head -n 5 | cut -f 1 -d " " | paste -d " " - - - - - | cut -f 5 -d " ")
    mapped_filtered=$(samtools flagstat $f | head -n 5 | cut -f 1 -d " " | paste -d " " - - - - - | cut -f 5 -d " ")
    mapped_filtered_rmdup=$(samtools flagstat ${pre}.rmdup.bam | head -n 5 | cut -f 1 -d " " | paste -d " " - - - - - | cut -f 5 -d " ")
    total=$(cat ${base}.bam.flagstat |  grep "paired in sequencing" | cut -f 1 -d " ")
    lib=$(basename $base)
    echo -ne "$lib\t$mapped_filtered_rmdup\t$mapped_filtered\t$mapped\t$total\n"
done | \
    awk 'BEGIN{OFS="\t"}{print $1, $2, $3, $4, $5, 100*$2/$5, 100*$3/$5, 100*$4/$5 }' >> ${BAMDIR}/${B}_OVERALLMAPSUMMARY.tsv

