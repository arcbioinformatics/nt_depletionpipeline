bam=$1
#cutoff=$2
samtools view -h $bam | \
    perl -ne 'use List::Util qw(sum); 
    @temp = split;
    @g = ${temp}[5] =~ m/(\d+)S/g;  
    $g1 = $g2 = 0;    
    $g1 = $1 if ${temp}[5] =~ m/^(\d+)S/;
    $g2 = $1 if ${temp}[5] =~ m/(\d+)S$/;

    @t = ${temp}[5] =~ m/(\d+)[MINSPX]/g; 
    @b = $_; 
    chomp @b; 
    $sums = 0;
    $sums = $g1 + $g2; 
    $sumt = sum(@t); 
    $seq = substr(${temp}[9], $g1, $sumt - $sums); 
    if($sumt != 0) {
        print "@b\tZS:i:$sums\tZT:i:$sumt\tZM:Z:$seq\n";
    }
    else {
        print $_
    }
    ' | \
    samtools view -hbS -  
