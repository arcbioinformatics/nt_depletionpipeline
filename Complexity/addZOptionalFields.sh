#!/bin/bash

if [ "$#" -ne 1 ]; then
    printf "\n This script will append 4 fields to a bam file.\n\n ZS: total number of soft clips in alignment \n ZT: total number of bases in read \n ZM: mapped (sub)read \n ZE: entropy of mapped (sub)read as used by Arc Informatics \n\nProvide bam file as input\n\n"
    exit 1;
fi



#BASEDIR="/mnt/bdbStorage01/sradhakrishnan/pipelines/filterBam"
BASEDIR="${NT_DEP_SCRIPT_DIR}/Complexity"
bam=$1
OUT_FOLDER=$(dirname $bam)
pre=`basename $1 .bam`
bash ${BASEDIR}/zs_zt.sh $bam > ${OUT_FOLDER}/${pre}.zszt.bam
paste -d "\t" <(samtools view ${OUT_FOLDER}/${pre}.zszt.bam) <(samtools view ${OUT_FOLDER}/${pre}.zszt.bam |  grep -o "ZM:Z:.*" | cut -f 1 -d " " | cut -f 3 -d ":" | ${BASEDIR}/entropy.R) | \
    cat <(samtools view -h ${OUT_FOLDER}/${pre}.zszt.bam | grep "^@") - | \
    samtools view -hbS - > ${OUT_FOLDER}/${pre}.zszt.ent.bam
rm -f ${OUT_FOLDER}/${pre}.zszt.bam
