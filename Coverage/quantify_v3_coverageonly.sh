if [ "$#" -ne 3 ]; then
    printf "\n Run command as: bash quantify_v3.sh <files_paired> <design_file> <config.tsv>\n"
    printf "\n Make sure files_paired has fastq.gz paired end read files, R1 first, followed by R2 separated by a space. Talk to Srihari if you don't know what a design file looks like. Also, check the config file if it contains all references you need to map to\n"
    exit 1;
fi

#INPUTS; see example in TEST_INPUTS folder
FASTQS=$1 #file with fastq paths, one pair per line separated by a space
DESIGN=$2 #3 column file qualifying each sample, noting its control set and treatment
CONFIG_FILE=$3

BWA_SCRIPT="/mnt/bdbStorage01/sradhakrishnan/pipelines/bwa/bwa_generic.sh"
SCRIPT_DIR="/tars01/sradhakrishnan/git/nt_depletionpipeline"
#CONFIG_FILE="/mnt/bdbStorage01/sradhakrishnan/pipelines/MixtureLibs/v3/test_config.tsv"
GNU_PARALLEL_EXEC="/home/sradhakrishnan/programs/parallel-20170822/bin/parallel"


map_sample() {
    DIR=$1
    REFERENCE=$2    
    FASTQS=$3
    BWA_SCRIPT=$4
    SCRIPT_DIR=$5
    DESIGN=$6
    SPECIES=$7

    echo "Running BWA in parallel - check ${DIR}.log and ${DIR}.err for updates/issues"
    if [[ ! -d "$DIR" ]]; then
       BWA_COMMAND="$BWA_SCRIPT $FASTQS $REFERENCE $DIR"
       eval "$BWA_COMMAND" # >> ${DIR}.log 2 >> ${DIR}.err
       bash $BWA_SCRIPT $FASTQS $REFERENCE $DIR >> ${DIR}.log 2 >> ${DIR}.err
    else
        echo "Skipping BWA for ${DIR} since folder already exists"
    fi

    echo "Generating mapped count data from BWA"
    RAWDATA=${DIR}_RAWDATA.tsv
    bash ${SCRIPT_DIR}/Preprocess/prepare_v3.sh $DESIGN $DIR > $RAWDATA 2> ${DIR}_RAWDATA.ERR

    echo "Writing genome coverage files..."
    SAMPLEDIR=${DIR}_SUBSAMPLED_
    REFERENCE_GENOMEFILE=${REFERENCE}.genome
#    if [[ ! -d $SAMPLEDIR ]] && [[ $SPECIES != "human" ]] ; then
    if  [[ $SPECIES != "human" ]] ; then
        bash ${SCRIPT_DIR}/Coverage/coveragePlots_v3.sh $DIR $REFERENCE_GENOMEFILE $SAMPLEDIR $SPECIES
        echo -ne "Plotting coverage plots..."
        Rscript ${SCRIPT_DIR}/Coverage/coveragePlots_v3.R $SAMPLEDIR $DESIGN $SPECIES
        echo "done"
    else
       echo "Reusing genome coverage data from $SAMPLEDIR since it already exists"
    fi

}

export -f map_sample
$GNU_PARALLEL_EXEC --record-env
$GNU_PARALLEL_EXEC --link -j3 map_sample ::: `cat $CONFIG_FILE | awk '{print $2}'` ::: `cat $CONFIG_FILE | awk '{print $3}'` ::: $FASTQS ::: $BWA_SCRIPT ::: $SCRIPT_DIR ::: $DESIGN ::: `cat $CONFIG_FILE | awk '{print tolower($1)}'`


#compute fold change for non-human genomes in the mixture in parallel
compute_FC() {
    DIR=$1    
    SCRIPT_DIR=$2
    SPECIES=$3

    RAWDATA=${DIR}_RAWDATA.tsv
    echo "Computing and plotting fold change with $SPECIES"
    Rscript ${SCRIPT_DIR}/compute_generic_nonhuman_v3.R $RAWDATA $PWD $SPECIES
}

export -f compute_FC
$GNU_PARALLEL_EXEC --record-env
#$GNU_PARALLEL_EXEC -j0 compute_FC ::: `cat $CONFIG_FILE | awk '(tolower($1)!="human") {print $2}'` ::: $SCRIPT_DIR ::: `cat $CONFIG_FILE | awk '(tolower($1)!="human"){print $1}'` 

## compute human depletion

HUMAN_DIR=`cat $CONFIG_FILE | awk '(tolower($1)=="human") {print $2}'`
HUMAN_RAWDATA=${HUMAN_DIR}_RAWDATA.tsv
#Rscript ${SCRIPT_DIR}/compute_generic_human_v3.R $HUMAN_RAWDATA $PWD 


#Rscript ${SCRIPT_DIR}/compute_generic.R $ECOLI_RAWDATA $HUMAN_RAWDATA $PWD
#
#echo "...done. Concatenating plots into ALL.pdf, check out FC_PLOT.pdf, HUMAN_PLOT.pdf and EC_PLOT.pdf for the individual plots"
#
#convert -density 300 +append FC_PLOT.pdf HUMAN_PLOT.pdf FC_HUMAN.pdf
#convert -density 300 -append FC_HUMAN.pdf EC_PLOT.pdf ALL.pdf
#rm -f FC_HUMAN.pdf
#
#
#echo -ne "...done. Concatenating plots into AllCoveragePlots.pdf..."
#convert -density 300 +append LibraryCoveragePlots.pdf  CoveragePlotsByControlSetMeans.pdf temp.pdf  
#convert -density 300 -append temp.pdf CoveragePlotsByExperimentMeans.pdf AllCoveragePlots.pdf
#rm -f temp.pdf
#echo "...done. Check out LibraryCoveragePlots.pdf, CoveragePlotsByControlSetMeans.pdf and CoveragePlotsByExperimentMeans.pdf for the individual plot."
#echo "End of pipeline"
