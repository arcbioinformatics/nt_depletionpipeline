# This script takes as argument 1) the species directory with bwa maps and 2) the species genome file (tab delimited; contig name and length) and samples the minimum # of species reads present in all the samples.
# Next, bedtools genomecov is run on each of the bam files producing coverage graphs for the min. number of reads; this is then plotted.

Dir=$1
genomeFile=$2
sampleDir=$3
species=$4
#tmpDir="/tars01/sradhakrishnan/tmp_sort/"
 
mkdir -p ${sampleDir}
getRC() {
    base=`basename $1 .fa.sorted.mappedonly.rmdup.bam`
    dir=`dirname $1`
    species=$3
    echo "Getting read count for $1" > ${2}/${base}.${species}.OUT
    samtools view -b -F4 $1 > ${2}/${base}.${species}.bam
    samtools flagstat ${2}/${base}.${species}.bam | head -n 1 | cut -f 1 -d " " > ${2}/${base}.${species}.readcount

    #also output genomecov files for bam files so we can draw up plots without having to subsample
    bedtools genomecov -max 1000000 -ibam $1 -g $4 | awk '$1=="genome"' > ${dir}/${base}.fa.sorted.mappedonly.rmdup.genomecov.tsv 2>${dir}/${base}.fa.sorted.mappedonly.rmdup.genomecov.err 
    samtools flagstat $1 | head -n 5 | cut -f 1 -d " " | paste -d " " - - - - - | cut -f 5 -d " " > ${dir}/${base}.fa.sorted.mappedonly.rmdup.mappedreadcount
}
export -f getRC
parallel --record-env
parallel -j12 getRC ::: `ls ${Dir}/*mappedonly.rmdup.bam` ::: ${sampleDir} ::: ${species} ::: ${genomeFile}

echo -ne "Done extracting $species reads. Figuring out how many to sample..."

readsToSample=`cat ${sampleDir}/*.${species}.readcount | sort -n | head -n 1`
readsToSample_new=$((readsToSample / 10))
echo "...done. Sampling $readsToSample reads; using $genomeFile as reference genome file"

totalGenomelength=`cat ${genomeFile} | awk '{sum+=$2}END{print sum}'`
echo "Total genome length is $totalGenomelength"
mean_cov="$(($readsToSample * 75/$totalGenomelength))"
echo "Mean coverage depth is $mean_cov"

echo $mean_cov > ${species}.meancov

sample() {
    base=`basename $1 .bam`
    sampleDir=`dirname $1`
    bedtools sample -n $2 -i $1 | samtools sort -@ 10 -m 2G -T $4 > ${sampleDir}/${base}.sample${2}.sorted.bam 2> ${sampleDir}/${base}.sample${2}.err
    echo "Generating genomecov: bedtools genomecov -max 1000000 -ibam ${sampleDir}/${base}.sample${2}.sorted.bam -g $3"
    bedtools genomecov -max 1000000 -ibam ${sampleDir}/${base}.sample${2}.sorted.bam -g $3 | awk '$1=="genome"' > ${sampleDir}/${base}.genomecov.tsv 2>${sampleDir}/${base}.genomecov.err

}
export -f sample
parallel --record-env
parallel -j12 sample ::: `ls ${sampleDir}/*.${species}.bam` ::: ${readsToSample} ::: ${genomeFile} ::: ${tmpDir}

