### This script is a slight variation on the first coverage script I wrote: the idea is to do a one-one comparison between the control and treatment libraries, 
### rather than all at once. It features a nested parallel module, which works independently on each control-treatment set after working out the minimum mapped reads
### between the sets. 

Dir=$1
genomeFile=$2
sampleDir=$3
species=$4
design=$5
#tmpDir="/tars01/sradhakrishnan/tmp_sort/"
 
mkdir -p ${sampleDir}
getRC() {
    exps=$1
    design=$2
    sampleDir=$3
    mkdir -p $sampleDir
    species=$4
    mapDir=$5 
    genomeFile=$6
    tmpDir=$7
    control=$(cat $design | awk -v var=$exps '($3==var){print $2}' | sort | uniq)

    toSample=$(for f in $(find $mapDir -name "*.flagstat" | fgrep -f <(cat $design | awk -v var=$control '($2==var){print $1"_"}') - | sort); do
        cat $f | head -n 5 | tail -n 1 | cut -f 1 -d " ";
    done | sort -n | head -n 1) 
    echo "Sampling $toSample reads for $exps"
    totalGenomelength=`cat ${genomeFile} | awk '{sum+=$2}END{print sum}'`
    mean_cov="$(($toSample * 75 / $totalGenomelength))"
    echo $mean_cov > ${sampleDir}/${species}_${exps}.meancov
    
    runSampling() {
        toSample=$1
        mapDir=$2
        sampleDir=$3
        species=$4
        sample=$5
        tmpDir=$6
        genomeFile=$7
        bamToSample=$(find $mapDir -name "${sample}*mappedonly.bam")
        echo "Running bedtools on $bamToSample and sampling $toSample reads"
        bedtools sample -n $toSample -i $bamToSample | samtools sort -@ 10 -m 2G -T $tmpDir > ${sampleDir}/${sample}${species}_SAMPLEDREADS${toSample}.sorted.bam
        samtools flagstat ${sampleDir}/${sample}${species}_SAMPLEDREADS${toSample}.sorted.bam | head -n 5 | cut -f 1 -d " " | paste -d " " - - - - - | cut -f 5 -d " " > ${sampleDir}/${sample}${species}_SAMPLEDREADS${toSample}.mappedreadcount
        bedtools genomecov -max 1000000 -ibam ${sampleDir}/${sample}${species}_SAMPLEDREADS${toSample}.sorted.bam -g $genomeFile | grep genome > ${sampleDir}/${sample}${species}_SAMPLEDREADS${toSample}.genomecov

    }
    export -f runSampling
    parallel --record-env
    parallel -j12 runSampling ::: $toSample ::: $mapDir ::: $sampleDir ::: $species ::: `cat $design | awk -v var=$control '($2==var){print $1"_"}' | sort` ::: ${tmpDir} ::: ${genomeFile}


}
export -f getRC
parallel --record-env

parallel -j12 getRC ::: `cat $design | awk '($3!="0") {print $3}' | sort | uniq` ::: $design ::: ${sampleDir} ::: ${species} ::: $Dir ::: $genomeFile ::: $tmpDir



