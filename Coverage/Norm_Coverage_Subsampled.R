rm(list=ls())

#base_folder <- "/tars01/sradhakrishnan/data/SequencingRuns/run165/180521_NB501788_0165_AHYJJGAFXX_temp/Analysis/Analysis1_Zymo"
#design_file <- "../Analysis1_Zymo/design"
#readlength_file <- "../Analysis1_Zymo/files_paired_medianreadlength.tsv"
#config_file <- "../Analysis1_Zymo/config_zymo.tsv"

args <- commandArgs(trailingOnly=T)
if (length(args)!=4) {
    stop("Need input files", call.=FALSE)
}

base_folder <- args[1]
design_file <- args[2]
readlength_file <- args[3]
config_file <- args[4]

setwd(base_folder)

design <- read.table(design_file, header=F, stringsAsFactors=F)
design[design==0] <- "CONTROL"

colnames(design) <- c("LIBRARY", "CONTROL_SET", "EXPERIMENT")

readlength <- read.table(readlength_file, header=F)
colnames(readlength) <- c("LIBRARY", "READLENGTH")
readlength$lib <- gsub("(_S\\d+)","",readlength$LIBRARY)
config <- read.table(config_file, header=F)
colnames(config) <- c("SPECIES", "MAPFOLDER", "GENOMEFILE", "TYPE")

config <- config[config$TYPE!="human",][, c(1,2,4)]

norm_coverage <- do.call(rbind, mapply(function(species, folder) {
       genomecov_files <- list.files(paste(base_folder, folder, sep="/"), pattern="*.genomecov", full.names=TRUE)
        
       do.call(rbind, lapply(genomecov_files, function(cov_file) {
            library(DescTools)
            cov_2 <- read.table(cov_file, header=F)
            cov_1 <- cov_2[cov_2$V1=="genome",][,c(2,4,5)]
            mapped_read_file <- gsub(".genomecov", ".mappedreadcount", cov_file)
            colnames(cov_1) <- c("DEPTH", "GENOMELENGTH", "FRACTION")

            #ELIMINATE 0 DEPTH ROWS
            eliminate <- which(cov_1$DEPTH==0)
            if(length(eliminate)>0) {
            cov <- cov_1[-eliminate,]                     
            } else {
            cov <- cov_1
            }

            TOTAL_FRAC <- sum(cov$FRACTION)
            cov$NZD_FRACTION <- cov$FRACTION/TOTAL_FRAC
            cov$MAPPEDREADCOUNT <- read.table(mapped_read_file, header=F)$V1
            ret <- unlist(lapply(readlength$lib, function(f) {
                  b  <- grep(f, basename(cov_file))
                  ifelse(length(b)==0,0,b) 
            }))
            lib_s <- readlength$LIBRARY[which(ret==1)]
            cov$LIBRARY <- lib_s
            cov$LIBRARY_REP <- readlength$lib[which(ret==1)]
            cov$MEDIANREADLENGTH <- readlength[readlength$LIBRARY==lib_s,]$READLENGTH
            cov$MEANDEPTH <- cov$MAPPEDREADCOUNT*cov$MEDIANREADLENGTH/cov$GENOMELENGTH
            cov$NORMDEPTH <- cov$DEPTH/cov$MEANDEPTH
            cov$CUMUL_FRACTION <- cumsum(cov$NZD_FRACTION)
#            cov$CC_FRACTION <- cumsum(cov$CUMUL_FRACTION)
            extrap <- approx(cov$NORMDEPTH, cov$CUMUL_FRACTION, xout=c(1), method="linear")$y
            cov_sub <- cov[cov$NORMDEPTH<1,]
            cov$AUC <- AUC(c(cov_sub$NORMDEPTH,1), c(cov_sub$CUMUL_FRACTION,extrap))
            cov$SPECIES <- species
            cov
        }))

}, config$SPECIES, as.factor(paste0(config$MAPFOLDER, "_SUBSAMPLED_INDIVIDUAL")), SIMPLIFY=FALSE))
norm_coverage_all <- merge(norm_coverage, design, by.x="LIBRARY_REP", by.y="LIBRARY")


#sanitycheck to see if fractions add up to 1
library(plyr)
#temp <- ddply(norm_coverage, c("LIBRARY", "MAPPEDREADCOUNT"), summarise, S=sum(FRACTION), ND=sum(NORMDEPTH))
#scores <-  ddply(norm_coverage_all, c("SPECIES", "EXPERIMENT"), summarise, MEANSCORE=round(mean(SCORE),3))
#ddply(norm_coverage_all, c("SPECIES", "EXPERIMENT"), summarise, SC=unique(SCORE))
#temp2 <- norm_coverage_all[norm_coverage_all$SPECIES=="Lactobacillus" & norm_coverage_all$MAPPEDREADCOUNT==1553555,]
#temp3 <- ddply(norm_coverage_all, c("LIBRARY", "MAPPEDREADCOUNT"), summarise, S= 



##### AUC calculation
## we only need rows for which NORM_DEPTH <=1
library(plyr)
norm_coverage_all_sub <- norm_coverage_all[norm_coverage_all$NORMDEPTH<=1,]
norm_coverage_all_sub$INTERVALS <- cut(norm_coverage_all_sub$NORMDEPTH, breaks=seq(0,1,by=0.01), include.lowest=T)
#scores <- ddply(norm_coverage_all_sub, c("LIBRARY", "SPECIES", "EXPERIMENT", "CONTROL_SET", "INTERVALS"), summarise, MEANINTERVALSCORE=mean(FRACTION))
#scores_lib <- ddply(scores, c("LIBRARY", "SPECIES", "EXPERIMENT", "CONTROL_SET"), summarise, TOTALSCORE=sum(MEANINTERVALSCORE))
#scores_expt <- ddply(scores_lib, c("CONTROL_SET", "EXPERIMENT", "SPECIES"), summarise, MEANSCORE=mean(TOTALSCORE))

scores_summary <- unique(norm_coverage_all[,c("LIBRARY", "AUC", "SPECIES", "CONTROL_SET", "EXPERIMENT", "MEANDEPTH")])
scores_summary_EXPT <- ddply(scores_summary, c("SPECIES", "CONTROL_SET", "EXPERIMENT"), summarise, MEAN_AUC=round(mean(AUC),3), MINDEPTH=round(min(MEANDEPTH),1), MAXDEPTH=round(max(MEANDEPTH),1))
scores_summary_EXPT_CTRL <- subset(scores_summary_EXPT, EXPERIMENT=="CONTROL")
scores_summary_EXPT_DEPLETED <- subset(scores_summary_EXPT, EXPERIMENT!="CONTROL")

library(ggplot2)
W=15
H=20

##note we are dealing only with fraction terms for bases with non-zero depth here

lapply(unique(norm_coverage_all$CONTROL_SET), function(cset) {
        norm_coverage_all_cset <- norm_coverage_all[norm_coverage_all$CONTROL_SET==cset & norm_coverage_all$NORMDEPTH<=2.5,]
        labels_cset_ctrl <- subset(scores_summary_EXPT_CTRL, CONTROL_SET==cset)
        labels_cset_expt <- subset(scores_summary_EXPT_DEPLETED, CONTROL_SET==cset)

        H <- ifelse(length(unique(norm_coverage_all_cset$EXPERIMENT))>10,50,H)
       norm_coverage_all_cset$GROUP <-  paste(norm_coverage_all_cset$LIBRARY_REP, norm_coverage_all_cset$MAPPEDREADCOUNT) 

       titles <- lapply(unique(labels_cset_expt$SPECIES), function(s) {
            sub <- subset(labels_cset_expt, SPECIES==s)
            sub_c <- subset(labels_cset_ctrl, SPECIES==s)
            lab <- paste(sub$EXPERIMENT, "; ", s, " : AUC (D) = ", sub$MEAN_AUC, "; Diff = ", round(sub$MEAN_AUC - sub_c$MEAN_AUC,3), "; Depth ~ [",  sub$MINDEPTH, " , ", sub$MAXDEPTH, "]", sep="")
        })


       labels_cset_expt_modified <- data.frame(SPECIES=labels_cset_ctrl$SPECIES, LABEL= sapply(titles, function(t) { paste0(t, collapse="\n") }))

       facet_1 <- ggplot(norm_coverage_all_cset, aes(x=NORMDEPTH, y=CUMUL_FRACTION, group=GROUP, colour=EXPERIMENT)) +
            geom_line(size=0.25) +
            geom_vline(xintercept=1) +
            facet_grid(SPECIES ~ .) +
            geom_text(aes(x=1.1, y=0.1, hjust = 0, vjust=0, label = LABEL), data=labels_cset_expt_modified, inherit.aes=FALSE, fontface="bold") +
            geom_text(aes(x=1.1, y=0.4 + length(titles[[1]])*0.1, hjust = 0, vjust=1, label = paste("AUC (C) = ", MEAN_AUC, "; Depth ~ [", MINDEPTH, " , ", MAXDEPTH, "]", sep="")), data=labels_cset_ctrl, inherit.aes=FALSE, fontface="bold") +
#            geom_text(aes(x=1.5, y=ypos_d, hjust = 0, label=paste("AUC (D) = ", MEAN_AUC, "; Depth := [", MINDEPTH, " , ", MAXDEPTH, "]", sep="")), data=labels_cset_expt, inherit.aes=FALSE, fontface="bold") +
#            geom_text(aes(x=1.5, y=ypos_c, hjust = 0, label=paste("AUC (C) = ", MEAN_AUC, "; Depth := [", MINDEPTH, " , ", MAXDEPTH, "]", sep="")), data=labels_cset_ctrl, inherit.aes=FALSE, fontface="bold") +
#            geom_text(aes(x=1.5, y=0.45, hjust = 0, label=paste("D - C = ", labels_cset_expt$MEAN_AUC - labels_cset_ctrl$MEAN_AUC, sep="")), data=labels_cset_ctrl, inherit.aes=FALSE, fontface="bold") +
            theme(legend.text=element_text(size=16), legend.position = "bottom",
                    panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
                    axis.title.x = element_text(face="bold", size=18),
                    axis.title.y = element_text(face="bold", size=18),
                    axis.text.x  = element_text(size=14, face="bold", hjust=1),
                    axis.text.y  = element_text(size=14, face="bold"),
                    strip.text.y  = element_text(size=16, face="bold"),
                    plot.margin = margin(0.5, 0.5, 0.5, 0.5, "cm")) +
             guides(colour = guide_legend(override.aes = list(size=3)))
            ggsave(paste(cset,"_NormCoveragePlots_cumulfraction_AUC_Subsampled.pdf", sep=""), facet_1, width=W, height=H, units="in", limitsize=FALSE)

        norm_coverage_all_exptsets <- norm_coverage_all[norm_coverage_all$CONTROL_SET==cset & norm_coverage_all$EXPERIMENT != "CONTROL" & norm_coverage_all$NORMDEPTH<=2.5,]
        norm_coverage_all_exptsets$GROUP <- paste(norm_coverage_all_exptsets$LIBRARY, norm_coverage_all_exptsets$MAPPEDREADCOUNT)
      facet_2 <- ggplot(norm_coverage_all_exptsets, aes(x=NORMDEPTH, y=CUMUL_FRACTION, group=GROUP, colour=SPECIES)) +
            geom_line(size=0.25) +
            geom_vline(xintercept=1) +
            facet_grid(EXPERIMENT ~ .) + 
            theme(legend.text=element_text(size=16), legend.position = "bottom",
                    panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
                    axis.title.x = element_text(face="bold", size=18),
                    axis.title.y = element_text(face="bold", size=18),
                    axis.text.x  = element_text(size=14, face="bold", hjust=1),
                    axis.text.y  = element_text(size=14, face="bold"),
                    strip.text.y  = element_text(size=16, face="bold"),
                    plot.margin = margin(0.5, 0.5, 0.5, 0.5, "cm")) + 
             guides(colour = guide_legend(override.aes = list(size=3)))
      ggsave(paste(cset,"_NormCoveragePlots_cumulfractiondepleted_BYSPECIES_Subsampled.pdf", sep=""), facet_2, width=W, height=H, units="in", limitsize=FALSE)
})



