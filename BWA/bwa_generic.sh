#!/bin/bash

if [ "$#" -ne 4 ]; then
    printf "\n Run command as: sh bwa.sh <filename> <absolute_bwa_index_path> <outdir> <host_type> \n"
    printf "\n Make sure filename has fastq.gz paired end read files, R1 first, followed by R2 separated by a space. \n"
    printf "\n Example: sh bwa_sh fastq_file bwa_index out_dir 2\n\n"
    exit 1;
fi


#f="files"
files=$1
g=$2
out_dir=$3
species_type=$4
#if [ -d "$out_dir" ]; then
#    printf "\n Output directory exists; remove it and rerun the command \n"
#    exit 1;
#else
    mkdir -p $out_dir
#fi
index_abbr=`basename $g`
while IFS= read line
do
	#echo "$line"
	IFS=' ' read -r -a array <<< "$line"
    THREADS=20
	f1="${array[0]}"
	f2="${array[1]}"
	pre_f=${f1/_R[12]_001.fastq.gz/}
	pre=`basename $pre_f`
	echo $pre
	#echo $f1
    #echo $f2
	if [ ! -f ${out_dir}/${pre}.${index_abbr}.sorted.mappedonly.bam ]; then	
		printf "\n Running BWA : Generating ${pre}.${index_abbr}.sorted.bam in folder ${out_dir} \n"
		bwa mem -k 23 -M -t $THREADS $g $f1 $f2 | samtools sort -@ $THREADS -m 10G -T temp - -o ${out_dir}/${pre}.${index_abbr}.sorted.bam 
		${NT_DEP_SCRIPT_DIR}/BWA/bamtools stats -in ${out_dir}/${pre}.${index_abbr}.sorted.bam -insert > ${out_dir}/${pre}.${index_abbr}.sorted.bam.stats 2> ${out_dir}/${pre}.${index_abbr}.sorted.bam.stats.err 
		samtools flagstat ${out_dir}/${pre}.${index_abbr}.sorted.bam > ${out_dir}/${pre}.${index_abbr}.sorted.bam.flagstat 2> ${out_dir}/${pre}.${index_abbr}.sorted.bam.flagstat.err
		#retain only mapped reads and delete the original file
		samtools view -h -F4 ${out_dir}/${pre}.${index_abbr}.sorted.bam | samtools view -hbs - > ${out_dir}/${pre}.${index_abbr}.sorted.mappedonly.bam
		samtools index ${out_dir}/${pre}.${index_abbr}.sorted.mappedonly.bam
		bash ${NT_DEP_SCRIPT_DIR}/Inserts/insertsize.sh ${out_dir}/${pre}.${index_abbr}.sorted.mappedonly.bam 
		if [ $species_type == "host" ]; then
			samtools view -h -f4 ${out_dir}/${pre}.${index_abbr}.sorted.bam | samtools view -hbs - | samtools sort -n -@ $THREADS -T temp -m 10G - -o ${out_dir}/${pre}.${index_abbr}.sortedn.unmapped.bam
			bedtools bamtofastq -i ${out_dir}/${pre}.${index_abbr}.sortedn.unmapped.bam -fq ${out_dir}/${pre}.${index_abbr}_unmapped_R1_001.fastq -fq2 ${out_dir}/${pre}.${index_abbr}_unmapped_R2_001.fastq
			cat ${out_dir}/${pre}.${index_abbr}_unmapped_R1_001.fastq | gzip > ${out_dir}/${pre}.${index_abbr}_unmapped_R1_001.fastq.gz 
			cat ${out_dir}/${pre}.${index_abbr}_unmapped_R2_001.fastq | gzip > ${out_dir}/${pre}.${index_abbr}_unmapped_R2_001.fastq.gz
			bash /mnt/bdbStorage01/programs/kraken/scripts/kraken_pair_compressed.bash ${out_dir}/${pre}.${index_abbr}_unmapped_R1_001.fastq.gz ${out_dir}/${pre}.${index_abbr}_unmapped_R2_001.fastq.gz
			

			rm -f ${out_dir}/${pre}.${index_abbr}_unmapped_R1_001.fastq ${out_dir}/${pre}.${index_abbr}_unmapped_R2_001.fastq
		else
			rm -f ${out_dir}/${pre}.${index_abbr}.sorted.bam
		fi
		bash ${NT_DEP_SCRIPT_DIR}/Inserts/removedups.sh ${out_dir}/${pre}.${index_abbr}.sorted.mappedonly.bam
	else
		printf "\n ${pre}.${index_abbr}.sorted.mappedonly.bam exists in folder ${out_dir}; not rerunning BWA mapping. \n"
	fi

done < "$files"

