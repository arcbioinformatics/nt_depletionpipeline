for f in $(find $(pwd -P) -type d -name "*bwamaps*"); do
	species=$(echo $(basename $f) | cut -f 1 -d "_")
	k=$(echo $(basename $f) | cut -f 3 -d "_")
	for g in $(find $f -name "*.flagstat"); do
		sample=$(echo $g | xargs -I xx basename xx | sed 's/_S.*//')
		mapped_reads=$(cat $g | sed -n '2,5p' | cut -f 1 -d " " | paste -d " " - - - -  | awk '{print $4-($1+$2)}')
		total_reads=$(cat $g | head -n 6 | tail -n 1 | cut -f 1 -d " ")
		echo $sample $species $mapped_reads $total_reads | awk 'BEGIN{OFS="\t";}{print $1, $2, $3, $4, 100*$3/$4}' 
	done
done | awk '{arr[$1]+=$5}END{for(i in arr){print i, 100-arr[i]}}' | sort -k1,1 -k 2,2n > Unmapped_reads.txt
Rscript ${NT_DEP_SCRIPT_DIR}/BWA/summarize.R
