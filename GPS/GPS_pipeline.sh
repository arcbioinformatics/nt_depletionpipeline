

## At this point, we have human bam files (mapped)
## We need to extract the non-human reads to pipe through chidotu

HUMAN_BAM_FOLDER=$1
. ~/venv/bin/activate
extract_unmapped() {
	BAM=$1
	PRE=$(basename $BAM .sorted.bam)
	DIR=$(dirname $BAM)
	#Using samtools -f 4 to get unmapped reads; and extracting corresponding mates as well for good measure.
	#Using samtools bam2fq to write out unmapped fastq files to pipe through chidotu
	if [ ! -f ${DIR}/${PRE}.unmapped.fastq.gz ]; then
		echo "generating unmapped read file ${DIR}/${PRE}.unmapped.fastq.gz"
		samtools view -f 4 $BAM | cut -f 1 | awk '!a[$1]++' | awk 'FNR==NR {hash[$1]; next}$1 in hash' - <(samtools view $BAM) | cat <(samtools view -H $BAM) - | samtools view -hbS - | samtools bam2fq - | gzip > ${DIR}/${PRE}.unmapped.fastq.gz
	fi
}
export -f extract_unmapped
parallel -j6 extract_unmapped ::: $(ls ${HUMAN_BAM_FOLDER}/*.sorted.bam)

find ${HUMAN_BAM_FOLDER} -name "*.unmapped.fastq.gz" | sort > ${HUMAN_BAM_FOLDER}/unmapped_fastq_paths.txt

## Calling pychidotu on human-removed fastqs, running against chidotu source to yield full.sam files

GPS_REF_PATH="${NT_DEP_SCRIPT_DIR}/GPS/refs/"
python ${NT_DEP_SCRIPT_DIR}/GPS/run_chidotu.py ${HUMAN_BAM_FOLDER}/unmapped_fastq_paths.txt $GPS_REF_PATH 

# NOW remove all the sorted.bam files 
rm -f ${HUMAN_BAM_FOLDER}/*.sorted.bam

## Now run the command to call evidence scores from the chidotu maps

for sample in $(find ${HUMAN_BAM_FOLDER}/GPS -mindepth 1 -maxdepth 1 -type d -name "*.unmapped"); do
	cp -r ${NT_DEP_SCRIPT_DIR}/GPS/refs/graphs ${sample}/
	python ${NT_DEP_SCRIPT_DIR}/GPS/evidence.py --input $sample --output ${sample}/strains/results.tsv > ${sample}_evidence.out 2>&1
done

regenerate_results() {
	PATHOGENS="${NT_DEP_SCRIPT_DIR}/GPS/refs/pathogens.txt"
	RESULTS=$1
	OUTPUT=${RESULTS/.tsv/}
	tail -n +5 $RESULTS | awk 'BEGIN{OFS="\t";}FNR==NR{hash[$2]=$1; next}{print hash[$1], $1, $2, $3, $4, $5 }' $PATHOGENS - > ${OUTPUT}_pathogenname.txt
}
export -f regenerate_results
parallel -j0 regenerate_results ::: $(find ${HUMAN_BAM_FOLDER} -name "results.tsv")


## Store one file that includes all results along with library names + library size (total reads, not read pairs)

awk 'BEGIN{OFS="\t";}FNR==NR{hash[$1]=$2; next}($1 in hash){print $0, hash[$1]}' <(find . -name "*RAWDATA.tsv" | sort | head -n 1 | xargs -I xx cat xx | awk '{print $1, $6}') <(for f in $(find . -name "*pathogenname*"); do library=$(echo $f | sed 's:.*GPS/\(.*\)_S[0-9][0-9]*.*:\1:'); cat $f | awk -vl=$library 'BEGIN{OFS="\t"}{print l, $0}'; done) | sort -k 1,1 | awk 'BEGIN{OFS="\t"}{print $0, $7*1000000/$8}' > library_pathogens_evpm.txt



