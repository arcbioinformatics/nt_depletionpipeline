import os
import getopt
import sys
import straincalling

#provide a file with library paths and make sure "graphs" folder exists in the library path
if __name__ == '__main__':

        input_path = '.'
        output_path = 'straincalling_results.tsv'

        try:
                opts, args = getopt.getopt(sys.argv[1:], 'i:o:', ['input=', 'output='])
        except getopt.GetoptError:
                print 'evidence.py -i <input_path> -e <epsilon> -t <targeted> -o <output_path>'
                sys.exit(2)

        for opt, arg in opts:
                if opt in ('-i', '--input'):
                        input_path = arg
                elif opt in ('-o', '--output'):
                        output_path = arg

        straincalling.call_strains(input_path, output_path)
