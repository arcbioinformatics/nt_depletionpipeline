import pychidotu.job as job
import os, string, sys
## Instantiate job class
run = job.AlignJob()

## Two arguments: 1. File with unmapped fastq file paths; one per line
##		  2. Path to reference files for chidotu mapping

file_paths = sys.argv[1]
ref_dir = sys.argv[2]
#ref_dir = "/tars01/sradhakrishnan/data/SequencingRuns/run334/Analysis/IC/human_bwamaps/chidotu_trial/"

with open(file_paths,"r") as unmapped_fastqs:
	for fastq in unmapped_fastqs:
		f = fastq.rstrip('\n')
		folder_name = os.path.basename(string.replace(f, ".fastq.gz",""))
		out_dir = os.path.dirname(f) + "/GPS/" + folder_name + "/alignment"
	    	if not os.path.exists(out_dir):
	        	os.makedirs(out_dir)
		run.prep(g = ref_dir + "CombinedRef.fa", v = ref_dir + "CombinedGraph.vcf", f = f, secondary = 1, e = "p", o = out_dir)
		run.run()
		print("Library " + folder_name + " done mapping via GPS")
