rm(list=ls())
#design_meta_file <- "design_meta"
args <- commandArgs(trailingOnly=T)
design_meta_file <- args[1]
design_meta <- read.table(design_meta_file, header=F, stringsAsFactors=F)
colnames(design_meta) <- c("PATH", "LABEL")
library(plyr)

meta_data <- ddply(design_meta, .(PATH, LABEL), function(run) {
	data <- read.table(paste(run$PATH, "Filtered_data.tsv", sep="/"), header=T, stringsAsFactors=F)
	data[data$EXPERIMENT!="CONTROL", c(4, 7, 8)]
	
})

remap_file <- paste(Sys.getenv("NT_DEP_SCRIPT_DIR"), "Meta/rename_expt", sep="/")
remap <- read.table(remap_file, header=F, stringsAsFactors=F)
colnames(remap) <- c("ORIGINAL_EXPT", "CORRECTED_EXPT")

meta_data_merge <- merge(meta_data, remap, by.x="EXPERIMENT", by.y="ORIGINAL_EXPT", all.x=T)
reset_rows <- which(is.na(meta_data_merge$CORRECTED_EXPT))
meta_data_merge$CORRECTED_EXPT[reset_rows] <- meta_data_merge$EXPERIMENT[reset_rows]

library(ggplot2)
W <- length(unique(meta_data_merge$CORRECTED_EXPT))*3
p <- ggplot(meta_data_merge) +
	geom_boxplot(aes(x=CORRECTED_EXPT, y=FOLD_CHANGE, colour=LABEL, group=CORRECTED_EXPT)) +
	geom_jitter(aes(x=CORRECTED_EXPT, y=FOLD_CHANGE, colour=LABEL, group=CORRECTED_EXPT)) +
	facet_grid(. ~ SPECIES) +
		 theme(legend.title=element_text(size=18, face="bold"), legend.text=element_text(size=16, face="bold"), legend.position = "bottom",
              axis.title.x = element_text(face="bold", size=18),
              axis.title.y = element_text(face="bold", size=18),
              axis.text.x  = element_text(size=14, face="bold", angle=45, hjust=1),
              axis.text.y  = element_text(size=14, face="bold"),
              strip.text.x  = element_text(size=14, face="bold"),
              strip.text.y  = element_text(size=14, face="bold"),
              plot.margin = margin(0.5, 0.5, 0.5, 0.5, "cm")) +
    labs(x="EXPERIMENT", y="FOLD CHANGE") +
        expand_limits(y=0)
ggsave(paste(paste(unique(meta_data$LABEL), collapse="_"), ".pdf", sep=""), p, width=W, height=15, units="in")
