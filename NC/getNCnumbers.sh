#input is bam directory
#output is one file per bam, with number of reads assigned to each of the 9 normalization control sequences, along with library size 

if [ "$#" -ne 1 ]; then
	echo "Need bwa maps folder"
	exit 1;
fi

MAPFOLDER=$1

getNCnumbers() {
	bam=$1
	outfolder=$(dirname $bam)
	pre=$(basename $bam .mappedonly.bam)
	total=$(cat ${pre}.bam.flagstat | grep "paired in sequencing" | cut -f 1 -d " ")
	echo -ne "LIBRARY\tSEQUENCE\tREADS\tTOTAL\tRPM\n" > ${outfolder}/${pre}_NCnumbers.tsv
	samtools view -f 2 $bam | awk -v total=$total -v library=$pre 'BEGIN{OFS="\t"}{arr[$3]++}END{for (i in arr){print library, i, arr[i], total, arr[i]*1000000/total}}' | grep NORMALIZATION | sort -k 1,1 >> ${outfolder}/${pre}_NCnumbers.tsv 
}
export -f getNCnumbers
parallel --record-env
parallel -j10 getNCnumbers ::: $(ls ${MAPFOLDER}/*.mappedonly.bam)

