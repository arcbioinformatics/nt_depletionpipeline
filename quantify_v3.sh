if [ "$#" -ne 4 ]; then
    printf "\n Run command as: bash quantify_v3.sh <files_paired> <design_file> <config.tsv> <host>\n"
    printf "\n Make sure files_paired has fastq.gz paired end read files, R1 first, followed by R2 separated by a space. Talk to Srihari if you don't know what a design file looks like. Also, check the config file if it contains all references you need to map to\n"
    exit 1;
fi


FASTQS=$1 #file with fastq paths, one pair per line separated by a space
DESIGN=$2 #3 column file qualifying each sample, noting its control set and treatment
CONFIG_FILE=$3
HOST_BEING_DEPLETED=$(echo $4 | awk '{print tolower($1)}')

present=$(cat ${NT_DEP_SCRIPT_DIR}/hosts | awk '{print $1}' | tr '\n' ' ' | grep -iw $HOST_BEING_DEPLETED - | wc -l)
if [ $present -eq 0 ]; then
        printf "Pipeline works with the following host species: \n\n$(cat ${NT_DEP_SCRIPT_DIR}/hosts | awk '{print $1}')\n"
        exit 1;
else
        echo "$(date +'%Y/%m/%d %H:%M:%S ')Running pipeline with host $HOST_BEING_DEPLETED"
fi

BWA_SCRIPT="${NT_DEP_SCRIPT_DIR}/BWA/bwa_generic.sh"
GNU_PARALLEL_EXEC="/tars00/sradhakrishnan/programs/parallel-20170822/bin/parallel"
export GNU_PARALLEL_EXEC

#get median read length
echo -ne "Computing median read length..."
bash ${NT_DEP_SCRIPT_DIR}/Preprocess/getMedianReadLength.sh $FASTQS
echo "$(date +'%Y/%m/%d %H:%M:%S ') done."

#print samples out
cat $FASTQS | cut -f 1 -d " " | xargs -I xx basename xx _R1_001.fastq.gz > samples
cp ${NT_DEP_SCRIPT_DIR}/run_monitoring/*.sh .


map_sample() {
    DIR=$1
    REFERENCE=$2    
    FASTQS=$3
    BWA_SCRIPT=$4
    NT_DEP_SCRIPT_DIR=$5
    DESIGN=$6
    SPECIES=$7
    HOST_TYPE=$8	

    echo "$(date +'%Y/%m/%d %H:%M:%S ')Running BWA in parallel - check ${DIR}.log and ${DIR}.err for updates/issues"
    #if [[ ! -d "$DIR" ]]; then
    BWA_COMMAND="$BWA_SCRIPT $FASTQS $REFERENCE $DIR $HOST_TYPE"
    eval "$BWA_COMMAND" # >> ${DIR}.log 2 >> ${DIR}.err
    #else
    #    echo "$(date +'%Y/%m/%d %H:%M:%S ')Skipping BWA for ${DIR} since folder already exists"
    #fi

       
    echo "$(date +'%Y/%m/%d %H:%M:%S ')Generating mapped count data from BWA"
    RAWDATA=${DIR}_RAWDATA.tsv
    RAWDATA_PE=${DIR}_RAWDATA_PE.tsv
    bash ${NT_DEP_SCRIPT_DIR}/Preprocess/prepare_v3.sh $DESIGN $DIR > $RAWDATA 2> ${DIR}_RAWDATA.ERR
    bash ${NT_DEP_SCRIPT_DIR}/Preprocess/prepare_PE.sh $DESIGN $DIR > $RAWDATA_PE 2> ${DIR}_RAWDATA_PE.ERR

    echo "$(date +'%Y/%m/%d %H:%M:%S ')Writing genome coverage files..."
    SAMPLEDIR=${DIR}_SUBSAMPLED
    REFERENCE_GENOMEFILE=${REFERENCE}.genome
    if [[ ! -d $SAMPLEDIR ]] && [[ $HOST_TYPE != "host" ]] ; then
        bash ${NT_DEP_SCRIPT_DIR}/Coverage/coveragePlots_v3.sh $DIR $REFERENCE_GENOMEFILE $SAMPLEDIR $SPECIES
        echo -ne "Plotting coverage plots..."
        Rscript ${NT_DEP_SCRIPT_DIR}/Coverage/coveragePlots_v3.R $SAMPLEDIR $DESIGN $SPECIES
        echo "$(date +'%Y/%m/%d %H:%M:%S ')done"
    else
       echo "$(date +'%Y/%m/%d %H:%M:%S ')Reusing genome coverage data from $SAMPLEDIR since it already exists"
    fi
    SAMPLEDIR_INDIVIDUAL=${DIR}_SUBSAMPLED_INDIVIDUAL
    if [[ ! -d $SAMPLEDIR_INDIVIDUAL ]] && [[ $HOST_TYPE != "host" ]] ; then
        bash ${NT_DEP_SCRIPT_DIR}/Coverage/coveragePlots_v3_individual.sh $DIR $REFERENCE_GENOMEFILE $SAMPLEDIR_INDIVIDUAL $SPECIES $DESIGN
        echo -ne "Plotting coverage plots individually..."
        echo "$(date +'%Y/%m/%d %H:%M:%S ')Command: Rscript ${NT_DEP_SCRIPT_DIR}/Coverage/coveragePlots_v3_individual.R $SAMPLEDIR_INDIVIDUAL $DESIGN $SPECIES"
        Rscript ${NT_DEP_SCRIPT_DIR}/Coverage/coveragePlots_v3_individual.R $SAMPLEDIR_INDIVIDUAL $DESIGN $SPECIES
        echo "$(date +'%Y/%m/%d %H:%M:%S ')done"
    else
       echo "$(date +'%Y/%m/%d %H:%M:%S ')Reusing genome coverage data from $SAMPLEDIR_INDIVIDUAL since it already exists"
    fi

}

export -f map_sample
$GNU_PARALLEL_EXEC --record-env
$GNU_PARALLEL_EXEC --link -j3 map_sample ::: `cat $CONFIG_FILE | awk '{print $2}'` ::: `cat $CONFIG_FILE | awk '{print $3}'` ::: $FASTQS ::: $BWA_SCRIPT ::: $NT_DEP_SCRIPT_DIR ::: $DESIGN ::: `cat $CONFIG_FILE | awk '{print tolower($1)}'` ::: `cat $CONFIG_FILE | awk '{print tolower($4)}'`

#plot map summary heatmap

#Rscript ${NT_DEP_SCRIPT_DIR}/Complexity/heatmap.R 

#summarize unmapped reads
bash ${NT_DEP_SCRIPT_DIR}/BWA/summarize.sh

#check if host is present in the config file. If yes, run FC, FG and depletion calculations and render the relevant plots
human_present=`awk 'tolower($4)=="host"' $CONFIG_FILE | wc -l`
echo $human_present
if [ "$human_present" -ne 0 ]; then
   #compute fold change for non-human genomes in the mixture in parallel
    compute_FC() {
        DIR=$1    
        NT_DEP_SCRIPT_DIR=$2
        SPECIES=$3
        NC_DIR=$4
    
        RAWDATA=${DIR}_RAWDATA.tsv
        echo "$(date +'%Y/%m/%d %H:%M:%S ')Computing and plotting fold change with $SPECIES"
        RCOMMAND_FC="Rscript ${NT_DEP_SCRIPT_DIR}/FC/compute_generic_nonhuman_v3.R $RAWDATA $PWD $SPECIES"
        echo "$(date +'%Y/%m/%d %H:%M:%S ')Running $RCOMMAND_FC"
        eval "$RCOMMAND_FC"    
    
	ZEST_CMD="bash ${NT_DEP_SCRIPT_DIR}/Complexity/filterBam_wrapper.sh $DIR"
	echo "$(date +'%Y/%m/%d %H:%M:%S ')runnign ZEST on $DIR"
	eval "$ZEST_CMD"
    }
    
    export -f compute_FC
    $GNU_PARALLEL_EXEC --record-env
    $GNU_PARALLEL_EXEC --link -j3 compute_FC ::: `cat $CONFIG_FILE | awk '(tolower($4)!="host") {print $2}'` ::: $NT_DEP_SCRIPT_DIR ::: `cat $CONFIG_FILE | awk '(tolower($4)!="host"){print $1}'` 
    
    #::: `cat $CONFIG_FILE | awk '(tolower($1)=="nc"){print $2}'` 
    
    ## compute host depletion
    HUMAN_DIR=`cat $CONFIG_FILE | awk '(tolower($4)=="host") {print $2}'`
    HUMAN_RAWDATA_PE=${HUMAN_DIR}_RAWDATA_PE.tsv
    HUMAN_RAWDATA=${HUMAN_DIR}_RAWDATA.tsv
#   Rscript ${NT_DEP_SCRIPT_DIR}/FC/compute_generic_human_v3.R $HUMAN_RAWDATA_PE $PWD 
    Rscript ${NT_DEP_SCRIPT_DIR}/FC/compute_generic_human_v3.R $HUMAN_RAWDATA $PWD 
    
    ## compute experiment specific numbers and plot
    Rscript ${NT_DEP_SCRIPT_DIR}/FC/compute_generic_nonhuman_treatmentwise.R $PWD

    ## collate all FC plots into one
    Rscript ${NT_DEP_SCRIPT_DIR}/FC/compute_nonhuman_all.R $CONFIG_FILE $PWD

    ## In case of contamination, compute FC relative to only host reads	
    Rscript ${NT_DEP_SCRIPT_DIR}/FC/compute_nonhost_relative_to_host.R $CONFIG_FILE $PWD
    RCOMMAND_ZEST="Rscript ${NT_DEP_SCRIPT_DIR}/Complexity/FC_filtered.R $DESIGN $CONFIG_FILE $PWD"
    eval "$RCOMMAND_ZEST"
 
    ## compute host insert sizes
    IS_COMMAND="Rscript ${NT_DEP_SCRIPT_DIR}/Inserts/insertSizePlots.R $DESIGN $PWD $CONFIG_FILE"
    echo "$(date +'%Y/%m/%d %H:%M:%S ')Running $IS_COMMAND"
    eval $IS_COMMAND    

    #GPS running
#    echo "$(date +'%Y/%m/%d %H:%M:%S ')Running GPS on unmapped host reads..."
 #   bash ${NT_DEP_SCRIPT_DIR}/GPS/GPS_pipeline.sh ${HUMAN_DIR}
 #   echo "$(date +'%Y/%m/%d %H:%M:%S ')..done. Plotting graphs..."
 #   PATHOGENS_FILE="library_pathogens_evpm.txt"
 #   Rscript ${NT_DEP_SCRIPT_DIR}/GPS/FC_GPS.R $DESIGN $PATHOGENS_FILE $PWD

    #Diamond
 #   echo "$(date +'%Y/%m/%d %H:%M:%S ')Running Diamond on unmapped human reads..."
 #   bash ${NT_DEP_SCRIPT_DIR}/Diamond/diamond.sh ${HUMAN_DIR}/unmapped_fastq_paths.txt
 #   echo "$(date +'%Y/%m/%d %H:%M:%S ')...done"
   

     ##FG plots
    bed_host=$(cat ${NT_DEP_SCRIPT_DIR}/hosts | grep $HOST_BEING_DEPLETED | awk '{print $2}')	
    echo "$(date +'%Y/%m/%d %H:%M:%S ')Starting FG plot analysis..."
    bash ${NT_DEP_SCRIPT_DIR}/FG/guidesandfragments.sh $bed_host $HUMAN_DIR
    Rscript ${NT_DEP_SCRIPT_DIR}/FG/guidesandfragments.R $DESIGN $PWD 
    Rscript ${NT_DEP_SCRIPT_DIR}/FG/guidesandfragments_overlapsummary.R $DESIGN ${PWD}/fg_plots/overlapsummary

    ##PHD level stuff	
    if [ ! -d "PHD_plots" ]; then
        echo "$(date +'%Y/%m/%d %H:%M:%S ')Running PHD plots - this will be a while if your fastqs are bloated..."
        bash ${NT_DEP_SCRIPT_DIR}/PHD/phd.sh $FASTQS $CONFIG_FILE
    fi	
    Rscript ${NT_DEP_SCRIPT_DIR}/PHD/plotPHD.R $DESIGN $CONFIG_FILE ${PWD}/PHD_plots
    echo "$(date +'%Y/%m/%d %H:%M:%S ')Pipeline complete."	
fi


# COPY GRAPHS INTO FOLDERS AS NECESSARY

bash ${NT_DEP_SCRIPT_DIR}/CollateOutput/getFiles_output.sh $PWD
