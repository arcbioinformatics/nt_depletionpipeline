#if [ "$#" -ne 1 ]; then
#        echo "Provide tars folder with completed NT_DEPLETION_PIPELINE analysis"
#        exit 1;
#fi

CURRENT_FOLDER=$1
DESIGN="${NT_DEP_SCRIPT_DIR}/CollateOutput/output_design"
KEYGRAPHS="${NT_DEP_SCRIPT_DIR}/CollateOutput/output_keygraphs"
copyFiles() {
        CURRENT_FOLDER=$1
        PREFIX=$2
        SUFFIX=$3
        LOCAL_FOLDER=$4
	#echo $CURRENT_FOLDER $PREFIX $SUFFIX $LOCAL_FOLDER
        if [ $PREFIX == "0" ]; then PREFIX=""; fi
        if [ $SUFFIX == "0" ]; then SUFFIX=""; fi
        FILES=( $(find -L $CURRENT_FOLDER -name "*${PREFIX}*${SUFFIX}") )
#        echo $FILES
	FILES_LENGTH=${#FILES[@]}

        if [ $FILES_LENGTH -ne 0 ]; then
                mkdir -p $LOCAL_FOLDER
        	echo "Copying "*$PREFIX*$SUFFIX" in $CURRENT_FOLDER; moving to $LOCAL_FOLDER"
                for f in "${FILES[@]}"; do
                        cp ${f} ${LOCAL_FOLDER}/
                done
        fi
}
export -f copyFiles
parallel --record-env
parallel --link -j6 copyFiles ::: $CURRENT_FOLDER ::: $(cat "$DESIGN" | awk '{print $1}') ::: $(cat "$DESIGN" | awk '{print $2}')  ::: $(cat "$DESIGN" | awk '{print "OutputFiles/"$3}')

OUTFOLDER="${CURRENT_FOLDER}/OutputFiles"

copyKeyFiles() {
        FOLDER=$1
        SUBFOLDER=$2
        GRAPH=$3
	KEYGRAPHFOLDER="${FOLDER}/KeyGraphs"
	mkdir -p $KEYGRAPHFOLDER
        echo $FOLDER $SUBFOLDER $GRAPH
        cp ${FOLDER}/${SUBFOLDER}/$GRAPH ${KEYGRAPHFOLDER}
}
export -f copyKeyFiles
parallel --record-env
parallel -j0 copyKeyFiles ::: $OUTFOLDER ::: $(cat $KEYGRAPHS | awk '{print $1}') :::+ $(cat $KEYGRAPHS | awk '{print $2}')
